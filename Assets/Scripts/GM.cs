﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Difficulty
{
	Easy,
	Hard
}

public class GM : MonoSingleton<GM> 
{
	[SerializeField] private Toggle toggle;
	private Difficulty difficulty;

	private string[] words_Easy;
	private string[] words_Hard;

	public string[] GetWords()
	{
		switch (this.difficulty)
		{
			case Difficulty.Easy:
				return words_Easy;
			case Difficulty.Hard:
				return words_Hard;
			default:
				throw new UnityException("The difficulty enum is out of range");
		}
	}

	public void SetWords()
	{
		var splittedResponse = Server.Instance.Response.Split(new[] { "<br/>" }, System.StringSplitOptions.RemoveEmptyEntries);
		var easyText = splittedResponse[0];
		var hardText = splittedResponse[1];

		SetWords(easyText, Difficulty.Easy);
		SetWords(hardText, Difficulty.Hard);
	}

	private void SetWords(string text, Difficulty difficulty)
	{
		var words = text.Split(new[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);

		switch (difficulty)
		{
			case Difficulty.Easy:
				words_Easy = words;
				break;
			case Difficulty.Hard:
				words_Hard = words;
				break;
			default:
				break;
		}
	}

	public void OnPlayClick()
	{
		if (toggle.isOn)
			difficulty = Difficulty.Hard;

		else difficulty = Difficulty.Easy;

		SceneManager.LoadScene("Game");
	}
}
