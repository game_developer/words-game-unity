﻿using System.Text.RegularExpressions;

public static class WordHelper
{
	public static WordStruct GetWordStruct(string fullText)
	{
		MatchCollection m = Regex.Matches(fullText, @" ?\<.*?\>");

		WordStruct wordStruct = new WordStruct(Regex.Replace(fullText, @" ?\<.*?\>", string.Empty), 
			m[0].Value, 
			m[1].Value, m[1].Index);

		return wordStruct;
	}

	public static string GetFullText(WordStruct wordStruct)
	{
		string result = wordStruct.NoTagText;

		result = result.Insert(0, wordStruct.ColorTag).Insert(wordStruct.ClosingTagIndex + wordStruct.ColorTag.Length, 
			wordStruct.ClosingTag);


		return result;
	}

	public static string GetDefaultText(string fullText)
	{
		WordStruct wordStruct = GetWordStruct(fullText);
		wordStruct.ClosingTagIndex = 0;

		return GetFullText(wordStruct);
	}
}

public struct WordStruct
{
	public string NoTagText;

	public string ColorTag;

	public string ClosingTag;
	public int ClosingTagIndex;	

	public WordStruct(string noTagText, string colorTag, string closingTag, int closingTagIndex)
	{
		NoTagText = noTagText;
		ColorTag = colorTag;
		ClosingTag = closingTag;
		ClosingTagIndex = closingTagIndex;
	}
}