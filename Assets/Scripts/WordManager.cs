﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class WordManager : MonoSingleton<WordManager>
{
	#region Variables
	[SerializeField] private GameObject _wordPrefab;
	[SerializeField] private float _waitTime;

	private List<Word> currentWords = new List<Word>();
	private Vector2 _topPos;

	private string _input = "^";

	const string COLOR_END = "</color>";
	const string WHITE_COLOR = "<color=\"white\">";

	List<string> recentWords = new List<string>();

	public void AddToRecent(string word)
	{
		recentWords.Add(word);

		if (recentWords.Count > 10)
		{
			recentWords.Remove(recentWords.Last());
		}
	}

	float GetSpeed(float x)
	{
		if (x < 0)
			return 0;

		return 10f + x / 18f;
	}

	Vector2 GetRandomPos()
	{
		float height = Camera.main.orthographicSize;
		float width = height * Camera.main.aspect;

		_topPos = Vector2.up * (height + 0.4f);

		return _topPos + Vector2.right * Random.Range(-width + 1.5f, width - 1.5f);
	}

	string GetRandomWord()
	{
		List<string> allWords = GM.Instance.GetWords().ToList();
		List<string> availableWords = allWords.Except(recentWords).ToList();

		int randomIndex = Random.Range(0, availableWords.Count());

		return availableWords[randomIndex];
	}
	#endregion

	private void Awake()
	{
		for (int i = 0; i < 10; i++)
		{
			CreateWord();
		}

		StartCoroutine(LaunchWords_Coroutine());
	}

	private void Update()
	{
		string prevInput = _input;
		_input += Input.inputString;

		if (_input != prevInput)
		{
			Word[] fallingWords = (currentWords.Where(word => word.falling)).ToArray();

			int successCount = 0;

			//Every word that is falling
			foreach (Word word in fallingWords)
			{
				WordStruct wordStruct = WordHelper.GetWordStruct(word.Text);

				Match match = Regex.Match(wordStruct.NoTagText, _input);

				if (match.Success)
				{
					//Assuming it is </color> tag
					wordStruct.ClosingTagIndex = match.Length;

					if (wordStruct.NoTagText.Length == match.Length)
					{
						Reset();

						word.SimulateDestruction();
						SetLast(word);
					}

					word.Text = WordHelper.GetFullText(wordStruct);

					successCount++;
				}

			}

			if (successCount == 0)
			{
				Reset();
			}
		}


	}

	private void Reset()
	{
		_input = "^";
		SetAllDefault();
	}

	private void SetAllDefault()
	{
		foreach (Word word in currentWords)
		{
			word.Text = WordHelper.GetDefaultText(word.Text);
		}
	}

	private void CreateWord()
	{
		Word word = Instantiate(_wordPrefab, transform).GetComponent<Word>();
		word.Text += GetRandomWord();

		AddToRecent(word.Text);

		currentWords.Add(word);
	}

	private void SetLast(Word toSet)
	{
		toSet.Stop();

		currentWords.Remove(toSet);
		currentWords.Add(toSet);

		toSet.transform.position = GetRandomPos();
	}

	private void LaunchWord()
	{
		Word toLauch = currentWords.First();
		SetLast(toLauch);

		toLauch.Launch(GetSpeed(Time.timeSinceLevelLoad));
	}

	private IEnumerator LaunchWords_Coroutine()
	{
		while (true)
		{
			LaunchWord();

			yield return new WaitForSeconds(_waitTime);
		}
	}
}