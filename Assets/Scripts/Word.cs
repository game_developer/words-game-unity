﻿using UnityEngine;
using TMPro;

public class Word : MonoBehaviour 
{
	[HideInInspector] public float fallSpeed = 0;
	[HideInInspector] public bool falling = false;

	public static Word Active;
	
	TextMeshPro tmPro;

	public string Text
	{
		get { return tmPro.text; }
		set { tmPro.SetText(value); }
	}

	private void Awake()
	{
		tmPro = GetComponent<TextMeshPro>();
	}

	private void Update()
	{
		transform.Translate(Vector2.down * fallSpeed * Time.deltaTime / 10);
	}

	public void Launch(float fallSpeed)
	{
		this.fallSpeed = fallSpeed;
		falling = true;
	}

	public void Stop()
	{
		fallSpeed = 0;
		falling = false;
	}

	public void SimulateDestruction()
	{
		//Play destruction particle effects

		Debug.Log("Word got destroyed");
	}
}
