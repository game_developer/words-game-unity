﻿using System.Collections;
using UnityEngine;

public class Server : MonoSingleton<Server> 
{
	public string Response { get; private set; }

	void Awake()
	{
		const string url = "http://words/GetRequest.php";
		WWW www = new WWW(url);
		StartCoroutine(WaitForRequest(www));
	}

	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;

		if (www.error == null)
		{
			Response = www.text;

			GM.Instance.SetWords();
		}

		else
		{
			throw new UnityException("Unable to connect to server");
		}
	}
}
